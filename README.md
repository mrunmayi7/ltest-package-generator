# LTest Package Generator

A GUI to generate the necessary binary files for the LTest station. The GUI checks for correctness of files and generates the necessary binaries in a single user-specified location. 
## Getting Started

Download the LTest Package Generator executable file (.app) to run the GUI. 

Pick the Product Type from the dropdown menu and select the Version number. V2 will generate files with SHA encoding.

Browse for and fill in the file locations for the following binaries:
HS nRF Main, Lens STM Main, Lens STM ROM, Lens STM RAM, Lens TI Main, Lens TI Stack, STM co-processor (not required for M1)

Finally, fill out the Release# and hit Generate. The GUI will prompt you for a location to store the output files. 

Three output files will be generated: Package binary, Header binary (with SHA for V2), STM co-proc binary (except for M1)
